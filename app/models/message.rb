class Message < ActiveRecord::Base
  attr_accessible :content, :talker
  validates :content, :length => { :maximum => 10000 }
  scope :query_keyword, lambda{
    |keyword| where('talker like :q or content like :q', :q => "%#{keyword}%")
  }
end
