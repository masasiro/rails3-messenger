class MessagesController < ApplicationController
  @@keyword = nil
  def index
    @message = Message.new
    @messages = Message.query_keyword(params[:keyword]).order("created_at DESC").page(params[:page]).per(10)
    if params[:keyword].present?
      @@keyword = @keyword = params[:keyword]
    else
      @@keyword = @keyword = nil
    end
  end
  
  def create
    @message = Message.new(params[:message])
    if 0 < @message.content.length
      @message.talker = current_user.username
      if @message.save
        redirect_to messages_path(:keyword => @@keyword)
      else
        redirect_to messages_path, notice: 'Message Failed.'
      end
    else
      @message = nil
      redirect_to messages_path
    end
  end
  
  def edit
    @message = Message.find(params[:id])
  end
  
  def update
    @message = Message.find(params[:id])
    if current_user.username != @message.talker
      redirect_to messages_path, notice: "Can not edit other user's message."
      return
    end
    if @message.update_attributes(params[:message])
      redirect_to messages_path, notice: "Message Updated."
    else
      render action: 'edit'
    end
  end
  
  def destroy
    @message = Message.find(params[:id])
    if current_user.username != @message.talker
      redirect_to messages_path, notice: "Can not delete other user's message."
      return
    end
    @message.destroy
    redirect_to messages_path
  end
end
